$(function(){
    'use strict';

    var winWidth = $(window).width();
    var padding;
    if (winWidth > 960) {
        padding = 580;
    } else {
        padding = 490;
    }

    $('.gallery').css('padding-top', padding);
 
    //attach search button click handler
    function showPics(){
        $('.explain').fadeOut(200);
        $('body').addClass('searched');
         //remove previous images from previous search
        $('.pics').empty();
    
        //query the Flickr API
        $.getJSON('https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?',
        {
            tags: $('.tags').val(), 
            format: 'json'
        },
        function(data) {
            $.each(data.items, function(i,item){
                //append image to the grid, wrap with a link tag so it's clickable
                $('<img/>').attr('src', item.media.m).appendTo('.pics')
                .wrap('<a class="gallery-item" href=' + item.link.toString() + 'data-author="'+item.author+'"></a>');

                $('.title').html(data.title);
                $('.description').html(data.description);
            });
            $('.img-pic').attr('src', data.items[0].media.m);
            $('.img-info span').text(data.items[0].author);
            $('.main-img').fadeIn(200);
            $('.gallery-item').first().addClass('selected');
        });
    }

    function selectPic(src, author, $this){
        $('.selected').removeClass('selected');
        $this.addClass('selected');
        $('.img-pic').attr('src', src);
        $('.img-info span').text(author);
    }

    function navPic(direction){
        if(direction === 'next') {
            $('.selected').next('.gallery-item').trigger('click');
            $('.prev').show();
            if(!$('.selected').next('.gallery-item').length) {
                $('.next').hide();
            }
        } else {
            $('.selected').prev('.gallery-item').trigger('click');
             $('.prev').show();
            if(!$('.selected').prev('.gallery-item').length) {
                $('.prev').hide();
            }
        }
    }

    $('.image-nav').on('click', function(e){
        e.preventDefault();
        var direction = $(this).attr('data-role').substr(0,4);
        navPic(direction);
    });

    $(document).on('click', '.gallery-item', function(e) {
        e.preventDefault();
        var $this = $(this);
        var src = $this.find('img').attr('src');
        var author = $this.find('img').attr('data-author');
        selectPic(src, author, $this);
    });

    $('.searchbutton').on('click', function(e) {
        e.preventDefault();
        showPics();
    });
    $('.tags').on('keypress', function(e){
        if(e.which === 13){//Enter key pressed
            showPics();
        }
    });

    var target = 20,
    timeout = null;

    $(window).scroll(function () {
        if (!timeout) {
            timeout = setTimeout(function () {      
                clearTimeout(timeout);
                timeout = null;
                if ($(window).scrollTop() >= target) {
                    $('.header, .container').addClass('scrolled');
                } else if ($(window).scrollTop() === 0) {
                    $('.header, .container').removeClass('scrolled');
                }
            }, 250);
        }
    });

});